AWSMFA Helper
=============

A simple command-line node app to assist with MFA tokens and the AWS command-line API.

Run the program to output a series of 'export' statements that, via command substition (backticks or `$(foo)`) can directly set the AWS credential environment variables in the shell. Here's the help message. For easier use,
edit your `~/.aws/config` and add an `mfaSerial` data element under the appropriate profile or profiles with the
ARN of the MFA token you want to use.

```
$ awsmfa -h

  Usage: awsmfa [options]

  Pass an MFA token to AWS and return environment variables


  Options:

    -t, --token <token>       The MFA token to use, will prompt if absent (default: null)
    -d, --duration <seconds>  The duration to pass to AWS in seconds (default: 10800)
    -s --serial <arn>         The MFA serial number/ARN to pass to AWS, or will attempt to read from ~/.aws/config
    -p --profile              The profile in ~/.aws/credentials to use, $AWS_PROFILE or 'default' if unspecified
    -h, --help                output usage information
```

Sample usage, without specifying `-t` it will prompt for a token, then call AWS and write the new env vars:

```
$ awsmfa
Enter MFA Token: 477960
export AWS_ACCESS_KEY_ID='ASIA...JXCYXNA'
export AWS_SECRET_ACCESS_KEY='glwSB...+6KVjz7JZE'
export AWS_SESSION_TOKEN='FQoDYXd...orI6q0wU='
```

Sample usage, specify the token on the command line and you can directly pull in the environment via command substitition

```
$ eval `awsmfa -t 123456`
```

To install, assuming you have Node.js installed, download this repo and 
```
$ npm install -g
```

Here's a sample of adding `mfaSerial` to `~/.aws/config`:

```
[default]
output = json
region = us-west-2
mfaSerial = arn:aws:iam::97...39:mfa/matt.moore
```