#!/usr/bin/env node

const program = require('commander');
const ini = require('ini');
const AWS = require('aws-sdk');
const fs = require('fs');

program
    .version('0.0.1')
    .description('Pass an MFA token to AWS and return environment variables')
    .option('-t, --token <token>', 'The MFA token to use, will prompt if absent', null)
    .option('-d, --duration <seconds>', 'The duration to pass to AWS in seconds', 43200)
    .option('-s --serial <arn>', 'The MFA serial number/ARN to pass to AWS, or will attempt to read from ~/.aws/config')
    .option('-p --profile <profile>', 'The profile in ~/.aws/credentials to use', null)
    .option('-e --export', 'Include export shell directive in output')
    .parse(process.argv);

if (!program.profile) {
    if ("AWS_PROFILE" in process.env) {
        program.profile = process.env["AWS_PROFILE"];
    } else {
        program.profile = "default";
    }
}

AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: program.profile});
if (!program.serial) {
    const config = ini.parse(fs.readFileSync(`${process.env["HOME"]}/.aws/config`, 'utf-8'))
    const configName = program.profile==="default" ? "default" : `profile ${program.profile}`
    program.config = config[configName]
    program.serial = config[configName].mfaSerial
}

if (!program.token) {
    const readline = require('readline');
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.question("Enter MFA Token: ", (token) => {
        program.token = token;
        rl.close();
        getToken(program);
    });
} else {
    getToken(program);
}

function getToken(program) {
    const sts = new AWS.STS();
    const params = {
        DurationSeconds: Number(program.duration),
        SerialNumber: program.serial,
        TokenCode: program.token
    }
    sts.getSessionToken(params, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            process.stdout.write(`AWS_ACCESS_KEY_ID=${data.Credentials.AccessKeyId}\n`)
            process.stdout.write(`AWS_SECRET_ACCESS_KEY=${data.Credentials.SecretAccessKey}\n`)
            process.stdout.write(`AWS_SESSION_TOKEN=${data.Credentials.SessionToken}\n`)
            if (program.export) {
                process.stdout.write('export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN\n')
            }
            if (program.config && program.config.region) {
                // different AWS libs need AWS_REGION vs AWS_DEFAULT_REGION
                process.stdout.write(`AWS_REGION=${program.config.region}\n`)
                process.stdout.write(`AWS_DEFAULT_REGION=${program.config.region}\n`)
                if (program.config && program.config.export) {
                    process.stdout.write('export AWS_REGION AWS_DEFAULT_REGION\n')
                }
            }
        }
    });
}
